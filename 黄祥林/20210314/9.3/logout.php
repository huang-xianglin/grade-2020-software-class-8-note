<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <form method="post" action="logout_save.php">
        <table class="update login">
            <caption>
                <h3>登录</h3>
            </caption>
            <tr>
                <td>用户名：</td>
                <td><input name="user" type="text" /></td>
            </tr>
            <tr>
                <td>密码：</td>
                <td><input name="password" type="password" /></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="登录" class="btn" />
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>



<?php
$TaskId= $_GET['TaskId'] ?? null;

if (empty($TaskId)) {
    echo "任务id错误";
    die();
}

$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");
$sql = "delete FROM Task where TaskId=" . $TaskId;
$result = $db->exec($sql);
if ($result) {
    echo "删除班级成功<br />";
    echo "<a href=''>返回列表页面</a>";
} else {
    echo "删除班级失败，错误信息为：<pre>{$db->errorInfo()[2]}</pre>";
    echo "<a href='javascript:history.go(-1);'>返回上一页</a>";
}

create table class (
	Classid int primary key identity (1,1) not null,
	ClassName nvarchar(50) not null,
);
Alter table class add constraint UNQ_class_ClassName unique(ClassName);
create table Student (
	Studentid int primary key identity (1,1) not null,
	StudentName nvarchar(50) not null,
	StudentSex int not null default '3',
	StudentBirth date,
	StudentAddress nvarchar(255) not null default '',
	ClassId int not null
);
Alter table Student add StudentIdentityCard varchar(20) not null default '';
Alter table Student add constraint ClassId default '0';
Alter table Student add constraint StudentSex check(StudentSex=1 or StudentSex=2 or StudentSex=3)
create table Course (
	CourseId int,
	CourseName nvarchar(50) not null,
	CourseCredit int not null default '0',
);
Alter table CourseCredit  add constraint CourseCredit check(CourseCredit>0);
Alter table Course add constraint CourseName unique(CourseName);
create table ClassCourse (
	ClassCourseId int,
	ClassId int not null,
	CourseId int not null,
);
Alter table ClassCourse add constraint ClassId foreign key (ClassId) references Class(ClassId);
Alter table ClassCourse add constraint CourseId foreign key (CourseId) references Course(CourseId);
create table Score (
	ScoreId int,
	StudentId int not null,
	CourseId int not null,
	Score int not null,
);
Alter table Score add constraint score check(srore>=0);
Alter table Score add IsResit tinyint not null default '0'--是否是补考：0不是，1是
Alter table StudentId add constraint StudentId foreign key (StudentId) references StudentId(StudentId);
Alter table CourseId add constraint CourseId foreign key (CourseId) references CourseId(CourseId);
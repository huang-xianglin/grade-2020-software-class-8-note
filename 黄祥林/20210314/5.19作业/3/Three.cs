﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo3
{
    class Three
    {
        public string id;
        public string name;
        public int price;
        public string press;
        public string author;

        public int Price
        {
            get
             {
                return price;
            }
            set
            {
                if (value < 0)
                {
                    price = 0;
                }
                else
                {
                    price = value;
                }
            }
          }
             public void book()
        {
            Console.WriteLine("编号:" + this.id);
            Console.WriteLine("书名:" + this.name);
            Console.WriteLine("价格:" + this.price);
            Console.WriteLine("出版社:" + this.press);
            Console.WriteLine("作者:" + this.author );

        }
    }
}


//3.定义一个图书类，存放图书的编号、书名、价格、出版社、作者信息；
//对价格进行赋值限制，小于0价格，赋值为0
//在图书类中定义一个方法输出图书信息；
//在主方法实例化对象，赋值并输出
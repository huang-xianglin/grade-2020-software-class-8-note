﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo2
{
    class Two
    {
        public string id;
        public string name;
        public string sex;
        private int age;
        public string Professionalinformation;

        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (value < 0 || value > 128)
                {
                    age = 0;
                }
                else
                {
                    age = value;
                }
            }
        }
        public void student()
        {
            Console.WriteLine("学号:" + this.id);
            Console.WriteLine("姓名:" + this.name);
            Console.WriteLine("性别:" + this.sex);
            Console.WriteLine("年龄:" + this.age);
            Console.WriteLine("专业信息:" + this.Professionalinformation);

        }
    }
}


//2. 定义一个学生类，存放学生的学号、姓名、性别、年龄、专业信息；
//对年龄字段进行赋值的安全性设置，如果是非法值（小于0或者大于128岁），该年龄值为0；
//在学生类中定义一个方法输出学生信息。
//在主方法实例化对象，赋值并输出

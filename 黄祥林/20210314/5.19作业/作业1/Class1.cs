﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo1
{
    class Class1
    {
        public string userid;
        public string username;
        public string password;

        public void user()
        {
            Console.WriteLine("账号：" + this.userid);
            Console.WriteLine("用户名：" + this.username);
            Console.WriteLine("密码：" + this.password);
        }
    }
}


//1. 定义一个用户类，存放用户的账号、用户名和密码属性；
//在用户类中定义一个方法输出当前用户对象的账号、用户名和密码的信息；然后在主方法调用输出；
create table class (
	Classid int primary key identity (1,1) not null,
	ClassName nvarchar(50) not null,
);
create table Student (
	Studentid int primary key identity (1,1) not null,
	tudentName nvarchar(50) not null,
	tudentSex int not null default '3',
	StudentBirth date,
	StudentAddress nvarchar(255) not null,
	ClassId int,
);
create table Course (
	CourseId int,
	CourseName nvarchar(50) not null,
	CourseCredit int not null default '0',
);
create table ClassCourse (
	ClassCourseId int,
	ClassId int not null,
	CourseId int not null,
);
create table Score (
	ScoreId int,
	StudentId int not null,
	CourseId int not null,
	Score int not null,
);

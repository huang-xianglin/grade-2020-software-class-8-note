

import java.util.Scanner;

/*用户登录 改密码
 *书籍的信息管理（增加 删除 更新 查询(根据出版社查询  根据ISBN查询 根据价格范围查询  根据书名查询 根据作者查询)） 
 *出版社管理
 *用户管理
*/



public class Demo1 {

	static String[][] user = new String[10][4];

	static String[][] book = new String[20][5];

	static String[][] pubCom = new String[5][3]; // 出版社

	public static void main(String[] args) {
		// 初始化
		user[0][0] = "HP001";
		user[0][1] = "ken";
		user[0][2] = "123";
		user[0][3] = "admin";

		user[1][0] = "HP002";
		user[1][1] = "ryu";
		user[1][2] = "123";
		user[1][3] = "user";

		user[2][0] = "HP003";
		user[2][1] = "guile";
		user[2][2] = "123";
		user[2][3] = "user";

		book[0][0] = "001";
		book[0][1] = "JAVA高级编程";
		book[0][2] = "100";
		book[0][3] = "铁道部出版社";
		book[0][4] = "张三";

		book[1][0] = "002";
		book[1][1] = "SQLSERVER高级编程";
		book[1][2] = "150";
		book[1][3] = "清华出版社";
		book[1][4] = "李四";

		book[2][0] = "003";
		book[2][1] = "JAVA基础入门";
		book[2][2] = "50";
		book[2][3] = "铁道部出版社";
		book[2][4] = "张三";

		book[3][0] = "004";
		book[3][1] = "ORACLE高级编程";
		book[3][2] = "120";
		book[3][3] = "清华出版社";
		book[3][4] = "王五";

		pubCom[0][0] = "清华出版社";
		pubCom[0][1] = "武汉市XXXX";
		pubCom[0][2] = "aaa";

		pubCom[1][0] = "铁道出版社";
		pubCom[1][1] = "北京市XXXX";
		pubCom[1][2] = "bbb";

		pubCom[2][0] = "邮电出版社";
		pubCom[2][1] = "南京市XXXX";
		pubCom[2][2] = "ccc";


	
		Scanner s = new Scanner(System.in);
boolean loginSuc = true;
		while (loginSuc) {
		System.out.println("**************************************欢迎使用图书管理系统**************************************");
		
			System.out.println("请输入用户名：");
			String userName = s.next();
			System.out.println("请输入用户密码：");
			String userPwd = s.next();

			if (login(userName, userPwd)) {
				boolean p = true;
				while (p) {
					System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出系统");
					int c = s.nextInt();
					if (c == 1) {
						// 图书管理
						boolean t = true;
						while (t) {
							System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
							int k = s.nextInt();
							if (k == 1) {
								System.out.println("请输入图书ISBN:");
								String isbn = s.next();
								System.out.println("请输入书名:");
								String bookName = s.next();
								System.out.println("请输入价格:");
								String bookPrice = s.next();
								System.out.println("请输入出版社:");
								String pubCom = s.next();
								System.out.println("请输入作者:");
								String author = s.next();
								
								boolean b = insertBook(isbn, bookName, bookPrice, pubCom, author);
								if(b){
									System.out.println("添加成功！！！");
								}else{
									System.out.println("添加失败！！！该书已经存在！！！");
								}
								
							} else if (k == 2) {
								System.out.println("请输入要删除的ISBN：");
								String isbn = s.next();
								boolean b = delByIsbn(isbn);
								if(b){
									System.out.println("删除成功！！！");
								}else{
									System.out.println("删除失败！！！该isbn号无效！！！");
								}
							} else if (k == 3) {
								System.out.println("请输入ISBN号：");
								String isbn = s.next();
								int index = getIndexByISBN(isbn);
								if(index!=-1){
									System.out.println("请输入新的书名：");
									String bookName = s.next();
									System.out.println("请输入新的价格：");
									String bookPrice = s.next();
									System.out.println("请输入新的出版社：");
									String pubCom = s.next();
									System.out.println("请输入新的作者：");
									String author = s.next();
									//调用更新函数
									boolean b = updateBook(index, bookName, bookPrice, pubCom, author);
									if(b){
										System.out.println("更新成功！！！");
									}else{
										System.out.println("更新失败");
									}
								}else{
									System.out.println("该ISBN号不存在！！！");
								}
							} else if (k == 4) {
								boolean y = true;
								while (y) {
									System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级 ");
									int h = s.nextInt();
									if (h == 1) {
										System.out.println("请输入ISBN号：");
										String isbn = s.next();
										searchBookByISBN(isbn);
									} else if (h == 2) {
										System.out.println("请输入书名：");
										String bookName = s.next();
										searchByBookName(bookName);
									} else if (h == 3) {
										System.out.print("请输入出版社前的数字进行选择：");
										//打印所有的出版社
										printPC();
										int d = s.nextInt();
										
										if(d>=1&& d<=getNumOfPC()){   //用户输入的数字有效
											//根据输入的整数要找到对应的出版社名称
											String pcName = pubCom[d-1][0];
											//调用函数根据出版社名称查询
											searchBookByPC(pcName);
										}else{   //用户输入的数字超出范围
											System.out.println("请选择正确的数字！！！");
										}
									} else if (h == 4) {
										System.out.println("请输入作者姓名：");
										String author = s.next();
										searchBookByAuthor(author);
									} else if (h == 5) {
										System.out.println("请输入最低价格");
										int min = s.nextInt();
										System.out.println("请输入最高价格");
										int max = s.nextInt();
										searchBookByPrice(min, max);
									} else if (h == 6) {
										// 调用查询所有函数
										searchAllBook();
									} else if (h == 7) {
										y = false;
									} else {
										System.out.println("请输入正确的数字选项！！！");
									}
								}

							} else if (k == 5) {
								t = false;
							} else {
								System.out.println("请输入正确的选项数字！！！");
							}
						}
					} else if (c == 2) {
						// 出版社管理
						boolean t = true;
						while (t) {
							System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
							int k = s.nextInt();
							if (k == 1) {
								System.out.println("请输入出版社名称：");
								String pcName = s.next();
								System.out.println("请输入出版社地址：");
								String pcAddress = s.next();
								System.out.println("请输入出版社联系人：");
								String pcPerson = s.next();
								// b为false表示已经存在，所以添加失败
								boolean b = insertPC(pcName, pcAddress, pcPerson);
								if (b) {
									System.out.println("出版社添加成功");
								} else {
									System.out.println("出版社添加失败");
								}
							} else if (k == 2) {
								System.out.println("请输入要删除的出版社名称：");
								String pcName = s.next();

								boolean b = delByPCName(pcName);

								if (b) {
									System.out.println("删除成功");
								} else {
									System.out.println("删除失败");
								}

							} else if (k == 3) {
								// 更新出版社信息
								System.out.println("请输入要更新的出版社名称：");
								String pcName = s.next();
								int index = searchByPCName(pcName);
								//根据下标打印
								printPubCom(index);
								//接受用户输入新的信息(因为没有设置id，用name当主键,所以出版社名称不能改)
								System.out.println("请输入要更新的地址：");
								String pcAddress = s.next();
								System.out.println("请输入要更新的联系人姓名：");
								String pcPerson = s.next();
								//调用更新函数
								boolean b = updatePubCom(index, pcAddress, pcPerson);
								if(b){
									System.out.println("更新成功");
									//更新成功后打印最新数据
									printPubCom(index);
								}else{
									System.out.println("更新失败！！！");
								}
							} else if (k == 4) {
								System.out.println("请输入出版社名称：");
								String pcName = s.next();
								int index = searchByPCName(pcName);
								if (index != -1) {
									System.out.println("出版社名称：" + pubCom[index][0]);
									System.out.println("出版社地址：" + pubCom[index][1]);
									System.out.println("出版社联系人：" + pubCom[index][2]);
								} else {
									System.out.println("该出版社不存在！！！");
								}
							} else if (k == 5) {
								//查询所有出版社信息
								searchAllPubCom();
							} else if (k == 6) {
								t = false;
							}else {
								System.out.println("请输入正确的选项数字！！！");
							}
						}
					} else if (c == 3) {
						// 退出系统
						p = false;
						loginSuc = false;
						System.out.println("感谢使用本系统！！");
					} else {
						System.out.println("请输入正确的选项数字！！！");
					}
				}

			} else {
				System.out.println("该用户不存在或者密码错误！！！请重新登录！！！");
			}
		}
	}

	// 登录
	static boolean login(String userName, String userPwd) {
		boolean b = false;

		for (int i = 0; i < user.length; i++) {
			if (userName.equals(user[i][1]) && userPwd.equals(user[i][2])) {
				b = true;
				break;
			}
		}

		return b;
	}

	//查询所有出版社信息
	static void searchAllPubCom(){
		System.out.println("出版社名称\t地址 \t联系人\t");
		for (int i = 0; i < pubCom.length; i++) {
			if (pubCom[i][0] != null) {
				System.out.println(pubCom[i][0] + "\t" + pubCom[i][1] + "\t" + pubCom[i][2] + "\t");
			}
		}
	}
	
	//得到出版社的个数
	static int getNumOfPC(){
		int count = 0;
		
		for(int i = 0;i<pubCom.length;i++){
			if(pubCom[i][0]!=null){
				count++;
			}
		}
		
		return count;
	}
	
	//在用户选择出版社的时候动态输出所有出版社的信息
	static void printPC(){
		for(int i = 0;i<pubCom.length;i++){
			if(pubCom[i][0]!= null){
				System.out.print(i+1+". "+pubCom[i][0]+" ");	
			}
			
		}
		System.out.println();
	}
	
	// 出版社添加
	static boolean insertPC(String pcName, String pcAddress, String pcPerson) {
		boolean b = false;
		// 如果该出版社不存在
		if (searchByPCName(pcName) == -1) {
			// 查找第一个null的下标
			int index = getPCIndexOfFirstNull();
			// 在null下标上完成添加操作
			pubCom[index][0] = pcName;
			pubCom[index][1] = pcAddress;
			pubCom[index][2] = pcPerson;
			b = true;
		}

		return b;
	}

	// 查找书籍数组中第一个null的下标
		static int getBookIndexOfFirstNull() {
			int index = -1;

			for (int i = 0; i < book.length; i++) {
				if (book[i][0] == null) {
					index = i;
					break;
				}
			}

			return index;
		}
	
	// 查找出版社数组中第一个null的下标
	static int getPCIndexOfFirstNull() {
		int index = -1;

		for (int i = 0; i < pubCom.length; i++) {
			if (pubCom[i][0] == null) {
				index = i;
				break;
			}
		}

		return index;
	}

	//判断出版社是否在图书表中有关联信息
	static boolean isExistInBook(String pcName){
		boolean bool = false; //默认不存在
		
		for(int i = 0;i<book.length;i++){
			if(pcName.equals(book[i][3])){
				bool = true;
				break;
			}
		}
		
		return bool; 
	}
	
	// 出版社删除
	static boolean delByPCName(String pcName) {
		boolean b = false;
		int index = searchByPCName(pcName);
		// 如果出版社存在，并且没有关联的图书数据就删除
		if (index != -1) {
			if(!isExistInBook(pcName)){
				for(int i = index;i<pubCom.length-1;i++){
					pubCom[i] = pubCom[i+1]; 
				}
				b = true;	
			}else{
				System.out.println("该出版社有相关图书信息存在！不可以删除！如果要删除请先删除所有相关图书信息！！！");
			}
			
		}

		return b;
	}
	// 修改某下标的出版社信息(不含出版社名称)
	static boolean updatePubCom(int index , String pcAddress , String pcPerson){
		boolean b = false;
		
		pubCom[index][1] = pcAddress;
		pubCom[index][2] = pcPerson;
		b = true;    
		
		return b;
	}
	
	//根据下标打印出版社信息
	static void printPubCom(int index){
		System.out.println("出版社名称\t地址                    \t联系人\t");
		System.out.print(pubCom[index][0]+"\t");
		System.out.print(pubCom[index][1]+"\t");
		System.out.println(pubCom[index][2]+"\t");
	}
	
	// 根据出版社名称精确查询
	static int searchByPCName(String pcName) {
		int index = -1;

		for (int i = 0; i < pubCom.length; i++) {
			if (pcName.equals(pubCom[i][0])) {
				index = i;
				break;
			}
		}

		return index;
	}
	// 书籍增加
	static boolean insertBook(String isbn,String bookName , String bookPrice , String pubCom , String author) {
		boolean b = false;
		// 如果该书籍不存在
		int i = getIndexByISBN(isbn);
		if(i==-1){
			//找到空位置的下标
			int index = getBookIndexOfFirstNull();
			//赋值
			book[index][0] = isbn;
			book[index][1] = bookName;
			book[index][2] = bookPrice;
			book[index][3] = pubCom;
			book[index][4] = author;
			b = true;
		}
		return b;
	}
	
	// 书籍删除
	static boolean delByIsbn(String isbn) {
		boolean b = false;
		// 如果存在就删除
		int index = getIndexByISBN(isbn);
		if(index!=-1){
			for(int i = 0;i<book[index].length;i++){
				book[index][i] = null;
			}
			//删除成功
			b = true;
		}

		return b;
	}
	// 根据下标书籍更新(此处isbn号不能更新)
	static boolean updateBook(int index,String bookName , String bookPrice , String pubCom , String author){
		boolean b = false;
		
		book[index][1] = bookName;
		book[index][2] = bookPrice;
		book[index][3] = pubCom;
		book[index][4] = author;
		
		b = true;
		
		return b;
	}
	
	
	// 查询所有图书
	static void searchAllBook() {
		System.out.println("ISBN\t书名                       \t价格\t出版社           \t作者\t");
		for (int i = 0; i < book.length; i++) {
			if (book[i][0] != null) {
				System.out.println(book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
						+ book[i][4] + "\t");
			}
		}
	}

	//根据isbn号查询下标，默认不存在为-1
	static int getIndexByISBN(String isbn){
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if (isbn.equals(book[i][0])) {
				// 因为isbn是唯一的，所以退出循环
				index = i;
				break;
			}
		}
		return index;
	}
	
	// 根据isbn查询图书
	static void searchBookByISBN(String isbn) {
		System.out.println("ISBN\t书名                       \t价格\t出版社           \t作者\t");
		for (int i = 0; i < book.length; i++) {
			if (isbn.equals(book[i][0])) {
				System.out.println(book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
						+ book[i][4] + "\t");
				// 因为isbn是唯一的，所以退出循环
				break;
			}
		}
	}
	

	
	// 根据书名模糊查询图书
	static void searchByBookName(String bookName){
		System.out.println("\tISBN\t书名                       \t价格\t出版社           \t作者\t");
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]!=null && book[i][1].indexOf(bookName)!=-1) {
				System.out.println("\t" + book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
						+ book[i][4] + "\t");
			}
		}
	}
	
	
	
	
	// 根据出版社查询图书
	static void searchBookByPC(String pcName){
		System.out.println("\tISBN\t书名                       \t价格\t出版社           \t作者\t");
		for (int i = 0; i < book.length; i++) {
			if (pcName.equals(book[i][3])) {
				System.out.println("\t" + book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
						+ book[i][4] + "\t");
			}
		}
	}
	

	// 根据价格范围查询图书
	static void searchBookByPrice(int min, int max) {
		System.out.println("ISBN\t书名                       \t价格\t出版社           \t作者\t");
		for (int i = 0; i < book.length; i++) {

			if (book[i][0] != null) {
				int price = Integer.parseInt(book[i][2]);
				if (price >= min && price <= max) {
					System.out.println(book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
							+ book[i][4] + "\t");
				}
			}

		}
	}

	// 根据作者查询图书
	static void searchBookByAuthor(String author) {
		System.out.println("\tISBN\t书名                       \t价格\t出版社           \t作者\t");
		for (int i = 0; i < book.length; i++) {
			if (author.equals(book[i][4])) {
				System.out.println("\t" + book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
						+ book[i][4] + "\t");
			}
		}
	}

}

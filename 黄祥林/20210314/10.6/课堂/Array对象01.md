## 课前回顾

上节课我们学习了Math对象。Math对象不是一个构造器，所以我们不需要使用new就可以直接调用Math对象的属性和方法。

上节课的作业第1题，优化自定义的数学对象的max和min方法，使得调用这两个方法的时候，输出的结果和内置对象Math.max()和Math.min()方法的结果一致（表现行为一致）。请一个同学说说他的优化思路是什么。

## 本节课目标

1. 学习Date对象，了解如何获取时间戳，如何格式化时间信息。
2. 学习Array对象，掌握创建Array对象的常用方法

## Date对象

Date对象和Math对象不一样，它是一个构造函数。我们需要实例化后才能使用它的属性和方法。

Date对象我们主要用来处理日期和时间。

### 1. 创建Date对象

语法：

```js
let d = new Date(); // 返回当前系统时间
```

Date构造函数参数，常用的几种方式：

```js
// 1. 没有参数的形式
let d1 = new Date();
// 2. 字符串形式
let d2 = new Date('2021-10-06 10:10:00'); // 或'2021/10/06 10:10:00
// 3. 多参数形式
let d3 = new Date(2021, 10, 6);
```

> 注意：
>
> 移动页面开发中，在 iOS 系统中，new Date('2021-10-01')，这种以`-` 拼接的时间格式，构造器无法识别。建议在移动端页面开发的时候，时间格式使用以 `/` 拼接的字符串格式。

### 2. 日期格式化

如果我们想要获得`2021-10-06 15:00:00`这种格式的时间，该如何处理？

Date对象为我们提供了一系列的方法，获取日期和时间对应的值：

| 方法名        | 说明                                                  | 语法                  |
| ------------- | ----------------------------------------------------- | --------------------- |
| getFullYear() | 返回指定日期的年份                                    | dateObj.getFullYear() |
| getMonth()    | 返回指定日期的月份(0 ~ 11)                            | dateObj.getMonth()    |
| getDate()     | 返回一个指定日期为一个月中的哪一日（从1--31）         | dateObj.getDate()     |
| getDay()      | 返回一个具体日期中一周的第几天(0 ~ 6)，0 表示星期天。 | dateObj.getDay()      |
| getHours()    | 返回一个指定的日期对象的小时(0 ~ 23)                  | dateObj.getHours()    |
| getMinutes()  | 返回一个指定的日期对象的分钟数(0 ~ 59)                | dateObj.getMinutes()  |
| getSeconds()  | 返回一个指定的日期对象的秒数(0 ~ 59)                  | dateObj.getSeconds()  |

### 3. 获取总的毫秒数

Date 对象是基于1970年1月1日（世界标准时间）起的毫秒数 Date 实例用来处理日期和时间。

[为什么计算机起始时间从1970年开始？](https://blog.csdn.net/kang19940713/article/details/60466393)

我们经常利用总的毫秒数来计算时间，因为它更精确。

Date对象提供了两种方法来获取总的毫秒数：

```js
let date = new Date();
// 1. valueOf()
let time = date.valueOf(); 
// 2. getTime()
let time2 = date.getTime();

// 还有一个特别简洁的方式获取总的毫秒数，不过可能会有兼容性的问题
let time3 = +new Date();
```

### 4. 练习题

1. 自定义一个函数formatDate()，可以格式化时间信息。这个函数的参数可以有两种情况：一种是符合标准格式的时间字符串，一种是Date对象的实例。返回的时间格式效果： 2021-10-06 15:15:09 星期几

2. 倒计时函数：根据输入的时间（符合标准格式的时间字符串），给出当前时间距离输入时间的倒计时内容；
   	效果：距离某个时间，还有 x天x时x分x秒
   	
   	> 案例分析：
   	>
   	> ①核心算法：输入的时间减去现在的时间就是剩余的时间，即倒计时 ，但是不能拿着时分秒相减，比如 05 分减去25分，结果会是负数的。
   	>
   	> ②用时间戳来做。用户输入时间总的毫秒数减去现在时间的总的毫秒数，得到的就是剩余时间的毫秒数。
   	>
   	> ③把剩余时间的毫秒数转换为天、时、分、秒 （时间戳转换为时分秒）
   	>
   	> 转换公式：
   	>
   	> a)d = parseInt(总秒数/ 60/60 /24);  // 计算天数 
   	>
   	> b)h = parseInt(总秒数/ 60/60 %24)  // 计算小时
   	>
   	> c)m = parseInt(总秒数 /60 %60 );  // 计算分数 
   	>
   	> d)s = parseInt(总秒数%60); // 计算当前秒数 

## Array对象

### 1. 创建数组对象

创建数组对象有两种方法：

```js
// 1. 字面量方式
let arr = [];

// 2. 构造函数方式
let arr = new Array();
```

Array()构造函数有一个很大的缺陷，不同的参数个数会导致不一致的行为。生成新数组，直接使用字面量是更好的方法。

```js
let arr1 = new Array(2); // [empty*2] 创建一个长度为2，每个元素为空的数组
let arr2 = new Array(2.5); // 报错，非法的长度
let arr3 = new Array('abc'); // ?
let arr4 = new Array(1, 2); // 多个参数的时候输出什么内容？
```

### 2. 检测是否为数组对象

- instanceof 运算符，可以判断一个对象是否属于某种类型（常用与引用数据类型）

- Array.isArray() 用于判断一个对象是否为数组，isArray() 是比较迟退出方法，在浏览器的开发中会有兼容性的问题。

  isArray() 是Array对象的静态方法。语法

  ```
  Array.isArray(obj);
  ```

  - 参数 **obj** 是需要检测的值。
  - 返回值：如果obj是Array，则返回true，不是返回false

### 3. 添加删除数组元素

常用的添加删除数组元素的方法有：

| 方法名              | 说明                                                 | 返回值         |
| ------------------- | ---------------------------------------------------- | -------------- |
| push(参数1, ...)    | 一个或多个元素添加到数组的末尾(该方法修改原有数组)   | 该数组的新长度 |
| pop()               | 从数组中删除最后一个元素,此方法更改数组的长度        | 该元素的值     |
| unshift(参数1, ...) | 将一个或多个元素添加到数组的开头(该方法修改原有数组) | 数组的新长度   |
| shift()             | 从数组中删除第一个元素,此方法更改数组的长度          | 该元素的值     |

```js
let arr = ['red', 'green', 'blue'];
arr.push('yellow'); // 4，返回数组的长度
console.log(arr); // ['red', 'green', 'blue', 'yellow']
arr.pop(); // "yellow"，没有参数。返回被删除的元素

arr.unshift('black'); // 4, 返回当前数组的长度
arr.shift(); // "black", 没有参数，返回被删除的元素
```

> **思考**：
>
> push 和 pop 这一组方法和 unshift 和 shift 这一组方法对比，哪一组的性能比较好。

### 4. 数组排序的方法

| 方法名    | 说明                           | 返回值                         |
| --------- | ------------------------------ | ------------------------------ |
| reverse() | 将数组中元素的位置颠倒，无参数 | 该方法会改变元素组，返回新数组 |
| sort()    | 对数组的元素进行排序           | 该方法会改变元素组，返回新数组 |

#### 4.1 sort() 

##### 语法

```
arr.sort([compareFunction])
```

##### 参数

- **compareFunction**：**（可选）**，用来指定按某种顺序进行排列的函数。这个函数有两个参数：

  - **firstEl**：第一个用于比较的元素。
  - **secondEl**：第二个用于比较的元素。

   ```js
   arr.sort(function(a, b) {
     return a - b; // 升序
     // return b - a; // 降序  
   })
   ```

- 如果没有参数的情况：元素按照转换为的字符串的各个字符的Unicode位点进行排序。

  > 课外阅读：
  >
  > 1. 什么是ASCII码
  > 2. 什么是Unicode码

```js
let arr = ['apple', 'orange', 'banana'];
console.log(arr.sort()); // ？
```

### 5. 练习题

1. 有一个包含工资的数组[1500, 1200, 2000, 2100, 1800]，要求把数组中工资超多2000的删除，剩余的放到新数组里面。




----------------------------------
--连接查询
--1.内连接:inner join 
--2.外部连接:left/right/full outer join
----------------------------------


-------------------------------------------------------
--1.内连接：内连接仅仅返回存在字段匹配的记录,即符合连接条件的数据，无法满足的数据被过滤。
--------------------------------------------------------
--查询学生的班级信息：班级编号和班级名称，学号，姓名
select * from Student;
select * from Class;
select Student.ClassId,Class.ClassName,StudentId,StudentName from Student
inner join Class on Student.ClassId=Class.ClassId;
--查询学生的成绩信息：学号，姓名，课程编号，成绩
select * from Student;
select * from Score;
select Student.StudentId,StudentName,CourseId,Score from Student
inner join Score on Student.StudentId=Score.StudentId;
--查询学生选修的课程信息：学号，姓名，选修课程名称
select * from Student;
select * from Course;
select * from Score;
select Student.StudentId,StudentName,Course.CourseName from Student
inner join Score on Student.StudentId=Score.StudentId
inner join Course on Score.CourseId=Course.CourseId;
--查询学生的成绩信息：学号，姓名，课程名称，成绩
select * from Student;
select * from Score;
select * from Course;
select Student.StudentId,StudentName,Course.CourseName,Score from Student
inner join Score on Student.StudentId=Score.StudentId
inner join Course on Score.CourseId=Course.CourseId;
--查询选修了‘计算机基础’的学生信息：学号，姓名，课程名称，成绩
select * from Student;
select * from Score;
select * from Course;
select Student.StudentId,StudentName,Course.CourseName,Score from Student
inner join Score on Student.StudentId=Score.StudentId
inner join Course on Score.CourseId=Course.CourseId
where Course.CourseName='计算机基础';
---------------------------------------------------
--2.外连接：外部连接参与连接的表有主从之分，以主表的每行记录去匹配从表的记录，
--			符合连接条件的数据将直接返回结果集中，不符合连接条件的列将被填充NULL值后再返回结果集中。
---------------------------------------------------
--查询所有班级的学生信息：班级编号和班级名称，学号，姓名（有些班级可能没有学生）
select * from Student;
select * from Class;
select Class.ClassId,ClassName,StudentId,StudentName from Student
left join Class on Student.ClassId=Class.ClassId;
--查询所有班级的学生人数：班级名称，人数(没有学生的班级人数显示为0)
select * from Student;
select * from Class;
select Class.ClassName,count(*)人数 from Student
left join Class on Student.ClassId=Class.ClassId
group by Class.ClassName;
--查询所有班级的男女生人数：班级名称，性别，人数(没有学生的班级人数显示为0)
select * from Student;
select * from Class;
select Class.ClassName,StudentSex,count(*)人数 from Student
left join Class on Student.ClassId=Class.ClassId
group by Class.ClassName,StudentSex;
--查询所有学生的成绩信息：学号，姓名，课程编号，成绩（有些学生没有成绩）
select * from Student;
select * from Score;
select Student.StudentId,StudentName,CourseId,Score from Student
left join Score on Student.StudentId=Score.StudentId;
--查询所有学生的学号、姓名、选课总数、所有课程的总成绩,并按照总成绩的降序排列(没成绩的显示为 null )
select * from Student;
select * from Score;
select Student.StudentId,StudentName,count(CourseId),sum(Score) from Student
left join Score on Student.StudentId=Score.StudentId
group by Student.StudentId,StudentName
order by sum(Score) desc;
--查询所有课程的课程编号、课程名称、选修的学生数量，并按照学生数量的降序排列（没有成绩信息的课程，学生数为0）
select * from Student;
select * from Score;
select * from Course;
select Course.CourseId,CourseName,count(Student.StudentId) from Student
left join Score on Student.StudentId=Score.StudentId
left join Course on Score.CourseId=Course.CourseId
group by Course.CourseId,CourseName
order by count(Student.StudentId);


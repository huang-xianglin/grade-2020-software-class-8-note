create table AccountInfo(
AccountId int identity (1,1) not null,
AccountCode varchar(20) not null,
AccountPhone varchar(20),
RealName varchar(20) not null,
);
alter table AccountInfo add OpenTime smalldatetime not null;
alter table AccountInfo add constraint PK_AccountInfo_AccountId primary key (AccountId);
alter table AccountInfo add constraint UNQ_AccountInfo_AccountCode  unique(AccountCode );
alter table AccountInfo add constraint DF_AccountInfo_OpenTime default(getdate()) for OpenTime ;
alter table AccountInfo alter column AccountPhone varchar(20) not null;
create table BankCard(
CardNo varchar(30) primary key,--���п���
AccountId int not null,--�˻����
CardPwd varchar(30) not null,--��������
CardBalance money not null,--���п����
CardState tinyint not null,--���п�״̬��1������2��ʧ��3���᣻4ע����5˯��
CardTime varchar(30) not null,--����ʱ��
);
alter table BankCard alter column CardTime smalldatetime not null;
alter table BankCard add constraint DF_BankCard_CardTime default(getdate()) for CardTime;
alter table BankCard add constraint DF_BankCard_CardBalance default(0.00) for CardBalance;
alter table BankCard add constraint DF_BankCard_CardState default(1) for CardState;
alter table BankCard add constraint FK_BankCard_AccountId foreign key (AccountId) references AccountInfo(AccountId);
create table CardExchange(
ExchangeId int primary key identity (1,1),
CardNo varchar(30) not null,
MoneyInBank money not null,
MoneyOutBank money not null,
ExchangeTime smalldatetime not null,
);
alter table CardExchange add constraint FK_CardExchange_CardNo foreign key (CardNo) references  BankCard(CardNo);
alter table CardExchange add constraint FK_CardExchange_MoneyInBank check(MoneyInBank>=0);
alter table CardExchange add constraint FK_CardExchange_MoneyOutBank check(MoneyOutBank>=0);
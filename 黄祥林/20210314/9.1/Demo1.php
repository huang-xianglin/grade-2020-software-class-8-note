<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/9/1
 * Time: 10:10
 */
//使用正则表达式查询一个字符串是否全部由数字组成，并用php代码实现。
$str1 ="1234";
$result = preg_match("/^\d+$/", $str1);
if ($result) {
    echo $str1 . "符合条件";
} else {
    echo $str1 . "不符合匹配条件";
}

echo '<br>';
//使用正则表达式查询一个字符串是否全部由字母组成，并用php代码实现。
$str2 ="gsa";
$result = preg_match("/^[a-zA-Z]+$/", $str2);
if ($result) {
    echo $str2 . "符合条件";
} else {
    echo $str2 . "不符合匹配条件";
}

echo '<br>';
//使用正则表达式检测一个字符串是邮箱，并用php代码实现。
$str3 ="4341fdsf@163.com";
$result = preg_match("/^[0-9a-zA-Z]{1,}@[0-9a-zA-Z]{1,}\.[a-zA-Z]{1,}$/", $str3);
if ($result) {
    echo $str3 . "符合条件";
} else {
    echo $str3 . "不符合匹配条件";
}

echo '<br>';
//使用正则表达式判断一个字符串是手机号，并用php代码实现。
$str4 = "13859962286";
$result = preg_match("/^1\d{10}+$/", $str4);
if ($result) {
    echo $str4 . "符合条件";
} else {
    echo $str4 . "不符合匹配条件";
}

echo '<br>';
//实务中注册经常需要填写密码，我们需要对密码进行验证，参考如下规则，并用php代码实现。
//使用正则表达式判断密码只包含数字，并且长度在6~15位。
$str5 = "1234567";
$result = preg_match("/^\d{6,15}+$/", $str5);
if ($result) {
    echo $str5 . "符合条件";
} else {
    echo $str5 . "不符合匹配条件";
}

echo '<br>';
//使用正则表达式判断密码只包含数字和字母，并且既有数字也有字母，长度在6~15位。（高能预警）
$str6 = "123abcdeF";
$result = preg_match("/^[a-zA-Z0-9]{6,15}+$/", $str6);
$result1 = preg_match("/[a-zA-Z]/", $str6);
$result2 = preg_match("/[0-9]/", $str6);
if ($result&&$result1 &&$result2) {
    echo $str6 . "符合条件";
} else {
    echo $str6 . "不符合匹配条件";
}

echo '<br>';
//使用正则表达式判断密码只包含数字、字母和特殊字符!@#$%^&*，并且数字、字母、特殊字符至少包含1个，长度在6-15位。（高能预警）
$str7 = "1a!@#$%^&*";
$result = preg_match("/^[a-zA-Z0-9!@#$%^&*]{6,15}+$/", $str7);
$result1 = preg_match("/[a-zA-Z]/", $str7);
$result2 = preg_match("/[0-9]/", $str7);
$result1 = preg_match("/[!@#$%^&*]/", $str7);
if ($result) {
    echo $str7 . "符合条件";
} else {
    echo $str7 . "不符合匹配条件";
}


//上面的代码是用正则表达式进行判断，尝试不用正则表达式进行判断。（高能预警）
//提示：遍历字符串里面的每个字符，然后逐个进行判断。


